package tech.master.cursos;

import java.util.Optional;

import javax.annotation.PostConstruct;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Service
public class CursoService {
    @Autowired
    private CursoRepository repository;

    @PostConstruct
    public void popular() {
        Curso curso = new Curso();
        curso.setId(1);
        curso.setNome("Cobol Avançado Orientado a Objetos");

        repository.save(curso);
    }

    public Iterable<Curso> listar(){
        return repository.findAll();
    }

    public Optional<Curso> buscar(int id){
        return repository.findById(id);
    }
}
