package tech.master.cursos;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.server.ResponseStatusException;

@RestController
public class CursosController {
    @Autowired
    private CursoService service;

    @GetMapping
    public Iterable<Curso> listar(){
        return service.listar();
    }

    @GetMapping("/{id}")
    public Curso buscar(@PathVariable int id) {
        Optional<Curso> optional = service.buscar(id);

        if(optional.isPresent()) {
            return optional.get();
        }

        throw new ResponseStatusException(HttpStatus.NOT_FOUND);
    }
}
