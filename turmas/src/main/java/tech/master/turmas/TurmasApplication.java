package tech.master.turmas;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@SpringBootApplication
@EnableFeignClients
public class TurmasApplication {

    public static void main(String[] args) {
        SpringApplication.run(TurmasApplication.class, args);
    }

}
